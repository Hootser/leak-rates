
<h1>Για gasket πάχους 2 mm<h1>

#
| Definition                                 | Units        | Values                    | Comments                                 |
| ------------------------------------------ | ------------ | ------------------------- | ---------------------------------------- |
| Equation of leak rate for gasket           |              | {[(P/14.7)x(1/Tf)]^2}/150 |                                          |
| where                                      |              |                           |                                          |
| Final operating tightness parameters Tf    | dimenionless | 337246,18                 | According to the equation                |
| Gasket parameter Gb                        | psi          | 289                       | For GYLON 3510 for 1/16''(Garlock sheet) |
| Gasket parameter Gs                        | psi          | 6,61E-11                  | For GYLON 3510 for 1/16''(Garlock sheet) |
| Gasket parameter a                         | dimenionless | 0,274                     | For GYLON 3510 for 1/16''(Garlock sheet) |
| Initial gasket stress Sg                   | psi          | 10000                     | For T3 class                             |
| Operating gasket stress Sf                 | psi          | 5073,77                   | According to the equation                |
| Stress retention coefficient r             | dimenionless | 0,6                       | According to material (Table)            |
| Assembly efficiency n                      | dimenionless | 0,85                      |                                          |
| Internal pressure P                        | psi          | 17,63                     | Conversion 1.2 atm to psi                |
| Initial pressurized area of gasket Ai      | inches^2     | 7,294                     |                                          |
| Contact area of gasket Ag                  | inches^2     | 2,941                     |                                          |
| Slope of unloading curve in a ROTT test kf | -            | 2,51                      | According to the equation                |
| Tightness parameter Tp                     | dimenionless | 414153,33                 | According to the equation                |

| Leakage Lrm                                         | mg / sec / mm | 8,43111E-14 |                       |
|-----------------------------------------------------|---------------|-------------|-----------------------|
| Outer diameter of gasket d                          | inch          | 3,2126      | 2xPI() x outside radius |
| Lr                                                  | kg/hr         | 2,47672E-14 |                       |
| 1 hr to 1 year                                      | -             | 0,000114    |                       |
| Density  for T=7.5 C (Highest probable temperature) |  kg/ml        | 0,00000151  |                       |
| 11                                                  | ml            | 550,00      | Από Solidworks        |

| FINAL RESULTS            |            |           |
| ------------------------ | ---------- | --------- |
| Results for one lid      |            |           |
| Lr                       | ml/year    | 1,437E-04 |
| Lr                       | ml/sec     | 4,512E-12 |
|                          |            |           |
| Results for two lids     |            |           |
| Lr                       | ml/year    | 2,874E-04 |
| Lr                       | ml/sec     | 9,110E-12 |
|                          |            |           |
| Results for RH Seal      |            |           |
| Lr                       | ml/year    | 0,03      |
| Lr                       | ml/sec     | 9,510E-10 |
|                          |            |           |
| Results for check valve  |            |           |
| Lr                       | sccs       | 0,0000001 |
| Lr                       | ml/year    | 5,26      |
| Lr                       | ml/sec     | 0,0000001 |
|                          |            |           |
| Final value of leak rate |            |           |
| Lr                       | ml/year    | 5,290E+00 |
| Lr                       | ml/sec     | 1,010E-07 |
| Lr                       | L/sec      | 1,010E-10 |
| Lr                       | bar x L/sec  | 1,228E-10 |
| Lr                       | mbar x L/sec | 1,228E-13 |

<div style="page-break-after: always;"></div>

<h1>Πτώση πίεσης<h1>

#
| Definition                                             | Symbol-Units           | Value                   | Units   | Comments                                                                       |
|--------------------------------------------------------|------------------------|-------------------------|---------|--------------------------------------------------------------------------------|
| Εξωτερική πίεση                                        | P1                     | 121590                  | Pa      | Μετατροπή των 1.2 atm σε Pa                                                    |
| Εσωτερική πίεση                                        | P2                     | 101325                  | Pa      | Μετατροπή των 0.8 atm σε Pa                                                    |
| Όγκος payload container                                | V                      | 0,00055                 | m^3     | Από Solidworks                                                                 |
| Μάζα αέρα για 1.2 atm                                  | m1                     | 0,000830091             | kg      | From equation: (VxP1)/(RmxT)                                                  |
| Μάζα αέρα για 0.8 atm                                  | m2                     | 0,000691743             | kg      | From equation: (VxP2)/(RmxT)                                                  |
| Σταθερά αέρα                                           | Rm                     | 287,058                 | J/Kgxk  | -                                                                              |
| Θερμοκρασία payload container                          | T                      | 280,65                  | K       | Μετατροπή των 7.5 C σε K                                                       |
|                                                        |                        |                         |         |                                                                                |
|                                                        | WITHOUT VALVE          |                         |         |                                                                                |
| Mass loss without the pressure dropping below 0.8 atm  | Δm                     | 0,000138349             | kg      | Ελάχιστη μάζα αέρα που μπορεί να φύγει χωρίς να πέσει η πίεση κάτω από 0,8 atm |
| Mass loss for n years (2 lids + RH seal)               | Mass loss for n years  | 7,1987200E-08           | kg      | Final value of Lr(ml/year) * density * n years (from Table)                    |
|                                                        | WITH VALVE             |                         |         |                                                                                |
| Leak rate of valve                                     | Lr                     | 0,0000001               | ml/s    | Αυτό αλλαζεις και βλεπεις τη συνολικη μαζα του κελιου B17                      |
| Leak rate of valve                                     | Lr                     | 0,000000000000151000000 | kg/s    | Leak rate of valve x density                                                   |
| Leak rate of valve                                     | Lr                     | 4,76194E-06             | kg/year | Μετατροπή από kg/s σε kg/year                                                  |
| Mass loss for n years (2 lids + RH seal + valve)       | Mass loss for n years  | 9,52387E-06             | kg      | Leak rate of valve (kg/year) x n years                                         |
|                                                        |                        |                         |         |                                                                                |
|                                                        |                        |                         |         |                                                                                |
|                                                        |                        |                         |         |                                                                                |
| Total mass loss for n years (2 lids + RH seal + valve) | SUM of mass loss       | 9,595859200000E-06      | kg      | Sum of total masses                                                            |