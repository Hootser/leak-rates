Look into [[gasket sealers]]. **Να κάνουμε procure ένα gasket sealer for the lawlz**

Στο [excelοφυλλο](https://docs.google.com/spreadsheets/d/1ir1tnXOvkvfmheB5RfthOvUTakyuYv5roFjPYVlvPx8/edit#gid=1190775985) φαίνεται πως έχουμε payload container volume ισο με 5.14 mL

~~Επιλέχθηκε το PTFE with barium sulfate, διότι από το [Leak rate paper της Μαρίνας](file:///C:/Users/%CE%93%CE%B9%CF%8E%CF%81%CE%B3%CE%BF%CF%82/Desktop/AcubeSAT/Leak%20Rates/Leak%20rate%20paper%20(Marina's%20Training).pdf)  βγαίνει το πόρισμα πως το expanded PTFE with corrugated metal insert έχει πολύ χαμηλό leakage. 
[Εδώ](file:///C:/Users/%CE%93%CE%B9%CF%8E%CF%81%CE%B3%CE%BF%CF%82/Desktop/AcubeSAT/Leak%20Rates/Garlock%20GYLON%20Style%203510%20Spec%20Sheet%20-%20(NA)%202016-12%20EN.pdf) είναι τα στοιχεία του gasket που επιλέχθηκε.~~

(Ενδελεχή έλεγχο στο [Structural Analysis Report](file:///C:/Users/%CE%93%CE%B9%CF%8E%CF%81%CE%B3%CE%BF%CF%82/Desktop/AcubeSAT/Structural%20Analysis%20Report%20(Read).pdf) )

Με ποιο τρόπο θα βιδώσουμε τις βίδες που θα σφραγίσουν το gasket? Σύμφωνα με το link [αυτό](https://www.littlepeng.com/single-post/2017/07/13/flange-assembly-conditions) , θα πρέπει να ελέγξουμε την παράμετρο n. **ΝΑ ΕΠΑΝΑΛΗΦΘΟΥΝ ΟΙ ΥΠΟΛΟΓΙΣΜΟΙ ΜΕ n = 0.85**

[Εδώ](https://legacy.garlock.com/sites/default/files/documents/en/Garlock%20Gylon%20PTFE%20Packing%20Brochure%20A4.pdf) βρίσκονται όλα τα σχετικά προϊόντα Gylon της Garlock, μαζί με διάφορες γενικές πληροφορίες (και τρόπο βιδώματος του gasket). [Εδώ](https://www.garlock.com/userfiles/documents/products/ga/rl/oc/k_/gs/k_/3-/1_/ga/sk/garlock_gsk_3-1_gasketing_technical_manual_03-2017_lr_en-na.pdf) βρίσκονται ακόμα γενικότερα gasket προϊόντα της Garlock.

Για το Gasket Stress (Sg), τα 10^5 psi είναι κοντά στο μέγιστο επιτρεπτό όριο για το ROTT Test

- Σχετικά με εναλλακτικές μεθόδους έναντι του ROTT Test:
	"After the mid-1970s [...] ASME developed various technologies to predict seal performance for proper evaluation of gaskets so as to propose a more exact and efficient measurement standard of closeness property in gaskets and operating limits of asbestos-free gasket material. Since the 1970s, some new seal researches have been developed by industrial groups including the Pressure Vessel Research Council (PVRC) and the Materials Technology Institute of the Chemical Process Industries (MTI), that have started to work on the development of testing research in the gaskets. Accordingly, ROTT testing was developed in this period"
- Το PTFE χρησιμοποιείται συχνά ως παράδειγμα ενός καλού gasket.
[Source](http://users.cis.fiu.edu/~chens/PDF/leakage.pdf) 


[Η παραδοχή ότι μπορούμε να χρησιμοποιήσουμε τις εξισώσεις για στρόγγυλο gasket, ένω έχουμε τετράγωνο, είναι εύστοχη](https://www.engineersedge.com/fluid_flow/oring_leak_rate_13605.htm). 

[Κατάλληλη ασκούμενη δύναμη για το βίδωμα του gasket](https://www.pumpsandsystems.com/improved-torque-tension)

Το PTFE με θειικό βάριο που θα χρησιμοποιήσουμε θεωρείται [skived filled](https://www.google.com/search?client=opera-gx&q=is+ptfe+with+barium+sulfate&sourceid=opera&ie=UTF-8&oe=UTF-8) . ~~Σύμφωνα με το [Training της Μαρίνας](file:///C:/Users/%CE%93%CE%B9%CF%8E%CF%81%CE%B3%CE%BF%CF%82/Desktop/AcubeSAT/Leak%20Rates/Leak%20rate%20paper%20(Marina's%20Training).pdf), το filled PTFE παίρνει τιμή για τον συντελεστή r=0.7 ή 0.8~~,~~ΟΧΙ 0.6 που έχουμε θεωρήσει (απ'ότι φαίνεται έχει θεωρηθεί expanded PTFE)~~. Το skived filled PTFE (μάλλον αυτό θα χρησιμοποιήσουμε) είναι το gasket το οποίο θα κόψουμε εμεις (θα πάρουμε το φύλλο και θα του δώσουμε το σχήμα), ενώ το filled PTFE είναι κομμένο από τη μάνα του.

~~**Υπάρχει θέμα με το Tp**~~

Να ξεκινήσω να ελέγχω τις μεθόδους για Leak testing από [εδώ](https://www.flyability.com/leak-testing)

Να ρωτήσουμε τον leak expert για το αν το ROTT Test ενδύκνειται για αξιόπιστα αποτελέσματα σε συνθήκες διαστήματος.
Να στείλουμε στον David για τον Leak Expert.

Για ποιο λογο εχουμε 2 καπακια στο payload? (meeting)

Βιβλιογραφία:
- [GASKETING.NET](https://www.americansealandpacking.com/ptfe.html)
- [Υπολογισμός πυκνότητας αέρα εντός του container](https://www.toppr.com/guides/physics/fundamentals/density-of-air-how-to-calculate-air-density/)
- 