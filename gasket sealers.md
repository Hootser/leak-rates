**Benefits of gasket sealers:**
-   **Extra protection against leaks.** Using a gasket sealer can prevent leaks between surfaces. This is especially true for older engines where the surface may be pitted, gouged, or scraped, preventing a good seal and allowing leaks.
-   **Stronger mating.** When you use a gasket sealer properly, it can make a good seal even better because it can make the seal stronger. A stronger seal lasts longer and also allows parts to last longer. 
-   **Sealing ability.** Some applications, like oil pans and differential covers, require a gasket sealer. You don’t have to purchase a separate gasket because a gasket sealer is semi-liquid. It creates its own seal.

link: https://www.thedrive.com/reviews/31242/best-gasket-sealer

Το Gasket Sealer μπορεί μεν να αφαιρεθεί, αλλά είναι μεγάλος μπελάς.


